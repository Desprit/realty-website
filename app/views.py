from flask import Flask, render_template, redirect, jsonify
from flask import session, request, url_for, flash
from app import *
from config import LANGUAGES

@babel.localeselector
def get_locale():
	return request.accept_languages.best_match(LANGUAGES.keys())

@app.route('/')
@app.route('/index')
def index():
	return render_template('index.html')

@app.route("/gallery")
def gallery():
	return render_template("gallery.html")

