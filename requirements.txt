Babel==1.3
Flask==0.10.1
Flask-Babel==0.9
Jinja2==2.7.3
MarkupSafe==0.23
Werkzeug==0.9.6
argparse==1.2.1
gunicorn==19.0.0
itsdangerous==0.24
pytz==2014.4
speaklater==1.3
wsgiref==0.1.2