# -*- coding: utf-8 -*-
import os

LANGUAGES = {
    'it': 'Italian',
    'ru': 'Russian'
}

CSRF_ENABLED = True
SECRET_KEY = 'you shall not pass'
